import React, {useState} from "react";

export const LoginContext = React.createContext(null);

export default ({children}) => {
    const [loggedInUser, updatedLoggedUser] = useState();

    // @ts-ignore
    return <LoginContext.Provider value={[loggedInUser, updatedLoggedUser]}>
        {children}
    </LoginContext.Provider>;
}

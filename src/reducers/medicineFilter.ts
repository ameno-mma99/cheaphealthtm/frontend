const SET_FILTER = 'SET_FILTER';

export const updateFilter = (filterProp, filterValue) => ( {
    type: SET_FILTER,
    payload: { [filterProp]: filterValue }
} );

// export const updateFilter = (filter: MedicineFilter) => ( {
//     type: SET_FILTER,
//     payload: { [filterProp]: filterValue }
// } );

export enum OrderType {
    ASC, DESC
}

export interface MedicineFilter {
    region?: string,
    town?: string,
    minPrice?: number,
    maxPrice?: number,
    medType?: string[],
    startDate?: Date,
    endDate?: Date,
    name?: string,
    order: OrderType
}


const initialState: MedicineFilter = {
    order: OrderType.ASC
}

export default (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case SET_FILTER:
            return {
                ...state,
                ...payload,
            }
        default:
            return state;
    }
}

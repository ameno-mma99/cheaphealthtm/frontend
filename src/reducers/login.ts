import UserDB, { PharmacyDetails, User } from "../db/UserDB";

const LOGIN_ACTION = 'LOGIN_ACTION';
const LOGOUT_ACTION = 'LOGOUT_ACTION';
const UPDATE_PHARMACY_DETAILS = 'UPDATE_PHARMACY_DETAILS';
const UPDATE_ALL_PHARMACY_DETAILS = 'UPDATE_ALL_PHARMACY_DETAILS';

export const loginAction = (user) => ( {
    type: LOGIN_ACTION,
    payload: {
        isLoggedIn: true,
        authUser: user
    }
} )

export const logoutAction = () => ( {
    type: LOGOUT_ACTION,
    payload: {
        isLoggedIn: false,
        authUser: null
    }
} )

export const updatePharmacyDetails = (detailName, detailValue) => ( {
    type: UPDATE_PHARMACY_DETAILS,
    payload: { [detailName]: detailValue }
} )

export const updateAllPharmacyDetails = (details: PharmacyDetails) => ( {
    type: UPDATE_ALL_PHARMACY_DETAILS,
    payload: details
} )

interface LoginData {
    authUser: User,
    isLoggedIn: boolean
}

const initialState: LoginData = {
    authUser: null,
    isLoggedIn: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_ACTION:
        case LOGOUT_ACTION:
            return {
                ...state,
                ...action.payload
            }
        case UPDATE_ALL_PHARMACY_DETAILS:
        case UPDATE_PHARMACY_DETAILS:
            UserDB.updatePharmacy(state.authUser.username, action.payload);
            return {
                ...state,
                authUser: {
                    ...state.authUser,
                    pharmacyDetails: {
                        ...state.authUser.pharmacyDetails,
                        ...action.payload
                    }
                }
            }
        default:
            return state;
    }
}
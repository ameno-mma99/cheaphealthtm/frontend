import { ReactElement } from "react";

const TOGGLE_MODAL = 'TOGGLE_MODAL';

export const toggleModal = (payload: Modal) => ( {
    type: TOGGLE_MODAL,
    payload
} );

export interface Modal {
    content?: any | string | ReactElement,
    contentProps?: object,
    isOpen: boolean,
    closeOnClickOutside?: boolean,
}


export const initialState: Modal = {
    isOpen: false,
}

export default (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case TOGGLE_MODAL:
            return {
                ...state,
                ...payload,
            }
        default:
            return state;
    }
}

import { combineReducers } from 'redux';
import notificationReducer from "./notification";
import loginReducer from "./login";
import medicineFilter from "./medicineFilter";
import modalReducer from "./modalReducer";

export default combineReducers({
    notificationReducer,
    loginReducer,
    medicineFilter,
    modalReducer,
});
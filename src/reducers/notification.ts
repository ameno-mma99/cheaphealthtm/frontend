import { ReactElement } from "react";

const TOGGLE_NOTIFICATION = 'TOGGLE_NOTIFICATION';

export const toggleNotification = (payload: Notification) => ( {
    type: TOGGLE_NOTIFICATION,
    payload
} );

export enum NotificationType {
    SUCCESS, ERROR, INFO
}

export interface NotificationContent {
    text: string,
    actionElement: ReactElement
}

export interface Notification {
    content: string | NotificationContent | ReactElement,
    type: NotificationType,
    expireTime?: number,
    isOpen: boolean,
    closeOnClick?: boolean
}


export const initialState: Notification = {
    content: null,
    type: NotificationType.INFO,
    expireTime: null,
    isOpen: false,
}

export default (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case TOGGLE_NOTIFICATION:
            return {
                ...state,
                ...payload,
            }
        default:
            return state;
    }
}

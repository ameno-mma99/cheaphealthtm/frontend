import ButtonWithoutImage from "../components/buttons/ButtonWithoutImage";
import ButtonWithImage from "../components/buttons/ButtonWithImage";
import { useSelector } from "react-redux";
//@ts-ignore
import MedWithReceipt from "../assets/medWithReceipt.png";
//@ts-ignore
import MedNoReceipt from "../assets/medNoReceipt.png";
import './HomaPage.scss';

export default () => {
    // @ts-ignore
    const authState = useSelector(store => store.loginReducer);

    const { isLoggedIn } = authState;

    let isHidden = false;
    if (isLoggedIn === true) {
        isHidden = true;
    }

    return (
        <div className='home-container'>
            <ButtonWithImage
                text='Лекарства без рецепта'
                imageUrl={MedNoReceipt}
                to='/browse-medicine?recipe=0'
            />
            <ButtonWithImage
                text='Лекарства с рецепта'
                imageUrl={MedWithReceipt}
                to='/browse-medicine?recipe=1'
            />

            {!isLoggedIn && (
                <>
                    <ButtonWithoutImage
                        text='Register'
                        to='register'
                    />
                    <ButtonWithoutImage
                        text='Login'
                        to='login'
                    />
                </> )
            }
        </div>
    );
}
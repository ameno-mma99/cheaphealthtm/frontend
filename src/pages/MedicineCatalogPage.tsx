import MedicineCatalog from "../components/catalog/MedicineCatalog";
import FilterCatalog from "../components/catalog/FilterCatalog";
import './MedicineCatalogPage.scss';

export default () => (
    <div className='catalog-page-wrapper'>
        <FilterCatalog/>
        <MedicineCatalog/>
    </div>
);
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {browserHistory, HistoryRouter} from "./HistoryRouter";
import {Provider} from "react-redux";
import store from "./reduxStore";


ReactDOM.render(
    <React.StrictMode>
        <HistoryRouter history={browserHistory}>
            <Provider store={store}>
                <App/>
            </Provider>
        </HistoryRouter>
    </React.StrictMode>,
    document.getElementById('root')
);


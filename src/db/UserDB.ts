export interface Certificate {
    imageSrc: any,
    name: string
}

export interface PharmacyDetails {
    pharmacyImg?: string,
    address?: string,
}

export interface User {
    username: string,
    password: string,
    email: string,
    phone: string,
    certificate: Certificate,
    pharmacyDetails?: PharmacyDetails
}

export default ( () => {

    function getAllUsers(): User[] {
        const localStorageJson = localStorage.getItem('registeredUsers') || '[]';
        return JSON.parse(localStorageJson);
    }

    function registerUser(user: User): void {
        const allUsers = getAllUsers();
        allUsers.push(user);

        localStorage.setItem('registeredUsers', JSON.stringify(allUsers));
    }

    function getUserByUsernameOrEmail(username: string): User {
        return getAllUsers().find(user => user.username === username || user.email === username);
    }

    function updatePharmacy(username: string, pharmacyDetails: PharmacyDetails): void {
        const user = getUserByUsernameOrEmail(username);
        user.pharmacyDetails = {
            ...user.pharmacyDetails,
            ...pharmacyDetails
        }

        updateUser(user);
    }

    function updateUser(user: User) {
        const users = getAllUsers().map(u => {
            if (u.username === user.username || u.email === user.email) {
                return {
                    ...u,
                    ...user
                }
            }

            return u;
        })

        console.log(users)

        localStorage.setItem('registeredUsers', JSON.stringify(users));
    }

    return {
        registerUser: registerUser,
        getUser: getUserByUsernameOrEmail,
        getAllUsers: getAllUsers,
        updatePharmacy: updatePharmacy
    }
} )();
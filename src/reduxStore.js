import {applyMiddleware, compose, createStore as createReduxStore} from 'redux';
import reducers from './reducers';

const middleware = [
    // thunk,
    // transformUserMiddleware,
    // dimensionsChangedMiddleware
    // Chain more middleWares here
];

let composeEnhancers = compose;

if (process.env.NODE_ENV === 'development') {
    if (typeof window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ === 'function') {
        composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
    }
}

const createStore = () => createReduxStore(
    reducers,
    composeEnhancers(
        applyMiddleware(...middleware),
    ),
);


const store = createStore();

export default store;
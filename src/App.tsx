import React from 'react';
import './App.css';
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import { Route, Routes } from "react-router-dom";
import HomePage from "./pages/HomePage";
import RegistrationPage from "./pages/RegistrationPage";
import LoginPage from "./pages/LoginPage";
import MedicineCatalogPage from "./pages/MedicineCatalogPage";
import PharmacyDashboardPage from "./pages/PharmacyDashboardPage";
import Notification from "./components/notification";
import Modal from "./components/Modal";

function App() {
    return (
        <div className="app">
            <Notification/>
            <Modal/>
            <Header/>
            <div className='content'>
                <Routes>
                    <Route path='/' element={<HomePage/>}/>
                    <Route path='/register' element={<RegistrationPage/>}/>
                    <Route path='/login' element={<LoginPage/>}/>
                    <Route path='/browse-medicine' element={<MedicineCatalogPage/>}/>
                    <Route path='/dashboard' element={<PharmacyDashboardPage/>}/>
                </Routes>
            </div>
            <Footer/>
        </div>
    );
}

export default App;

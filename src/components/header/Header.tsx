import './Header.scss';
// @ts-ignore
import { ReactComponent as MedIcon } from '../../assets/medIcon.svg';
import { Link, useLocation } from "react-router-dom";
import AuthButton from "../AuthButton";
import { useDispatch, useSelector } from "react-redux";

export default () => {
    // @ts-ignore
    const authState = useSelector(store => store.loginReducer);
    const { pathname } = useLocation();

    const { isLoggedIn } = authState;
    console.log(pathname)
    const shouldCenterLogo = pathname === '/browse-medicine';

    return (
        <div className={`header ${shouldCenterLogo && 'centered-logo'}`}>
            <div className="med-icon">
                <Link to='/'>
                    <MedIcon/>
                </Link>
            </div>
            <div className='platform-name'>
                <p>Cheap Health TM</p>
            </div>
            {isLoggedIn && (
                <div className='auth-button-wrapper'>
                    <AuthButton/>
                </div> )}
        </div>
    );
}

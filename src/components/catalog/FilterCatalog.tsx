import { useEffect, useState } from "react";
import 'react-calendar/dist/Calendar.css';
import CalendarPopup from "../CalendarPopup";
import './FilterCatalog.scss';
import { useDispatch } from "react-redux";
import { updateFilter } from "../../reducers/medicineFilter";

export default () => {
    const dispatch = useDispatch();
    const [value, onChange] = useState(new Date());

    useEffect(() => {
        dispatch(updateFilter('startDate', value));
    }, [value]);


    const setFilter = (prop, value) => {
        dispatch(updateFilter(prop, value));
    }

    return (
        <div className='catalog-filter'>
            <div className="section-filter location-filter">
                <h3 className="section-title">Местоположение</h3>
                <input
                    type="text"
                    className="filter-input"
                    placeholder='Област'
                    onBlur={e => setFilter('region', e.target.value)}
                />
                <input
                    type="text"
                    className="filter-input"
                    placeholder='Град'
                    onBlur={e => setFilter('town', e.target.value)}
                />
            </div>
            <div className="section-filter price-filter">
                <h3 className="section-title">Цена</h3>
                <input
                    type="text"
                    className='filter-input'
                    placeholder='От: '
                    onBlur={e => setFilter('minPrice', e.target.value)}
                />
                <input
                    type="text"
                    className='filter-input'
                    placeholder='До: '
                    onBlur={e => setFilter('maxPrice', e.target.value)}
                />
            </div>
            <div className="section-filter medicine-filter">
                <h3 className="section-title">Тип лекарства</h3>
                <div className="checkbox-wrapper">
                    <label htmlFor="checkBox1">Тип лекарство 1</label>
                    <input
                        id='checkBox1'
                        type='checkbox'
                        className="input-checkbox"
                    />
                </div>
                <div className="checkbox-wrapper">
                    <label htmlFor="checkBox1">Тип лекарство 2</label>
                    <input id='checkBox1' type='checkbox' className="input-checkbox"/>
                </div>
                <div className="checkbox-wrapper">
                    <label htmlFor="checkBox1">Тип лекарство 3</label>
                    <input id='checkBox1' type='checkbox' className="input-checkbox"/>
                </div>
                <div className="checkbox-wrapper">
                    <label htmlFor="checkBox1">Тип лекарство 4</label>
                    <input id='checkBox1' type='checkbox' className="input-checkbox"/>
                </div>
                <div className="checkbox-wrapper">
                    <label htmlFor="checkBox1">Тип лекарство 5</label>
                    <input id='checkBox1' type='checkbox' className="input-checkbox"/>
                </div>
            </div>
            <div className="section-filter medicine-filter">
                <h3 className="section-title">Тип лекарства</h3>
                <CalendarPopup
                    label='След'
                    onChange={onChange}
                    value={value}
                />
            </div>
        </div>
    );
}
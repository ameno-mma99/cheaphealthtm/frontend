export interface Pharmacy {
    name: string,
    town: string,
    expirationDate: Date,
    price: number
}

export interface Medicine {
    imgUrl?: string,
    medName?: string,
    pharmacies?: Pharmacy[]
}

export const medicineMock: Medicine[] = [
    {
        medName: 'Aspirin',
        imgUrl: 'pill1',
        pharmacies: [
            {
                name: 'Apotheco',
                town: 'Dobrich',
                expirationDate: new Date(2022, 10, 30),
                price: 15.99
            },
            {
                name: 'Assured Rx',
                town: 'Varna',
                expirationDate: new Date(2022, 4, 12),
                price: 34.99
            },
            {
                name: 'Blink Health',
                town: 'Plovdiv',
                expirationDate: new Date(2022, 1, 15),
                price: 31.25
            },
            {
                name: 'GenScripts',
                town: 'Vidin',
                expirationDate: new Date(2022, 7, 21),
                price: 45.99
            }
        ]
    },
    {
        medName: 'Analgin',
        imgUrl: 'pill1',
        pharmacies: [
            {
                name: 'Blink Health',
                town: 'Dobrich',
                expirationDate: new Date(2022, 11, 10),
                price: 15.99
            },
            {
                name: 'MedSavvy',
                town: 'Sofia',
                expirationDate: new Date(2022, 2, 17),
                price: 8.99
            }
        ]
    },
    {
        medName: 'Paracetamol',
        imgUrl: 'pill1',
        pharmacies: [
            {
                name: 'Envolve Health',
                town: 'Vidin',
                expirationDate: new Date(2022, 4, 30),
                price: 4.99
            },
            {
                name: 'MobiMeds',
                town: 'Shumen',
                expirationDate: new Date(2022, 5, 12),
                price: 8.99
            },
            {
                name: 'CareFirst',
                town: 'Shumen',
                expirationDate: new Date(2022, 4, 21),
                price: 11.99
            }
        ]
    },
    {
        medName: 'Muskolizin',
        imgUrl: 'pill1',
        pharmacies: [
            {
                name: 'Envolve Health',
                town: 'Varna',
                expirationDate: new Date(2022, 2, 15),
                price: 34.99
            },
            {
                name: 'CareFirst',
                town: 'Varna',
                expirationDate: new Date(2022, 2, 30),
                price: 24.99
            },
            {
                name: 'MedImpact',
                town: 'Sofia',
                expirationDate: new Date(2022, 3, 2),
                price: 40.99
            },
            {
                name: 'Bioplus Specialty',
                town: 'Sofia',
                expirationDate: new Date(2022, 4, 21),
                price: 45.99
            }
        ]
    },
    {
        medName: 'Paramax',
        imgUrl: 'pill1',
        pharmacies: [
            {
                name: 'Blink Health',
                town: 'Dobrich',
                expirationDate: new Date(2022, 1, 25),
                price: 1.99
            },
            {
                name: 'CareFirst',
                town: 'Shumen',
                expirationDate: new Date(2022, 1, 30),
                price: 4.99
            }
        ]
    },
    {
        medName: 'Citomax',
        imgUrl: 'pill2',
        pharmacies: [
            {
                name: 'MobiMeds',
                town: 'Sofia',
                expirationDate: new Date(2022, 6, 8),
                price: 11.5
            },
            {
                name: 'GenScripts',
                town: 'Burgas',
                expirationDate: new Date(2022, 4, 3),
                price: 21.5
            }
        ]
    },
    {
        medName: 'Parafin',
        imgUrl: 'pill1',
        pharmacies: [
            {
                name: 'MobiMeds',
                town: 'Razgrad',
                expirationDate: new Date(2022, 7, 3),
                price: 20.99
            },
            {
                name: 'CareFirst',
                town: 'Varna',
                expirationDate: new Date(2022, 10, 30),
                price: 24.99
            }
        ]
    },
    {
        medName: 'Gripex-max',
        imgUrl: 'pill1',
        pharmacies: [
            {
                name: 'GenScripts',
                town: 'Sofia',
                expirationDate: new Date(2022, 3, 30),
                price: 5.55
            },
            {
                name: 'MedOne',
                town: 'Varna',
                expirationDate: new Date(2022, 4, 5),
                price: 5.01
            },
            {
                name: 'MedOne',
                town: 'Dobrich',
                expirationDate: new Date(2022, 4, 11),
                price: 5.01
            },
            {
                name: 'GenScripts',
                town: 'Shumen',
                expirationDate: new Date(2022, 2, 16),
                price: 4.01
            },
            {
                name: 'MedOne',
                town: 'Razgrad',
                expirationDate: new Date(2022, 1, 21),
                price: 6.01
            },
            {
                name: 'CareFirst',
                town: 'Sliven',
                expirationDate: new Date(2022, 2, 15),
                price: 11.01
            }
        ]
    },
    {
        medName: 'Statin',
        imgUrl: 'pill1',
        pharmacies: [
            {
                name: 'CareFirst',
                town: 'Sliven',
                expirationDate: new Date(2022, 6, 21),
                price: 32.99
            },
            {
                name: 'MedOne',
                town: 'Kaloyanovo',
                expirationDate: new Date(2022, 1, 30),
                price: 31.99
            }
        ]
    },
    {
        medName: 'Vazelin',
        imgUrl: 'pill1',
        pharmacies: [
            {
                name: 'MedSavvy',
                town: 'Sofia',
                expirationDate: new Date(2022, 5, 30),
                price: 21.99
            },
            {
                name: 'Envolve Health',
                town: 'Varna',
                expirationDate: new Date(2022, 6, 30),
                price: 17.99
            }
        ]
    },
    {
        medName: 'Statin',
        imgUrl: 'pill2',
        pharmacies: [
            {
                name: 'Envolve Health',
                town: 'Dobrich',
                expirationDate: new Date(2022, 8, 30),
                price: 51.99
            },
            {
                name: 'OptumRx',
                town: 'Haskovo',
                expirationDate: new Date(2022, 5, 15),
                price: 61.99
            }
        ]
    },
    {
        medName: 'Rozulin',
        imgUrl: 'pill2',
        pharmacies: [
            {
                name: 'MedSavvy',
                town: 'Sofia',
                expirationDate: new Date(2022, 10, 30),
                price: 5.99
            },
            {
                name: 'Envolve Health',
                town: 'Sofia',
                expirationDate: new Date(2022, 6, 30),
                price: 4.99
            }
        ]
    },
    {
        medName: 'Simgal',
        imgUrl: 'pill2',
        pharmacies: [
            {
                name: 'OptumRx',
                town: 'Razgrad',
                expirationDate: new Date(2022, 6, 30),
                price: 17.99
            },
            {
                name: 'PharmaMed',
                town: 'DimitrovGrad',
                expirationDate: new Date(2022, 10, 8),
                price: 24.99
            }
        ]
    },
    {
        medName: 'Liprimar',
        imgUrl: 'pill2',
        pharmacies: [
            {
                name: 'OptumRx',
                town: 'Dobrich',
                expirationDate: new Date(2022, 6, 30),
                price: 5.99
            },
            {
                name: 'PharmaMed',
                town: 'Varna',
                expirationDate: new Date(2022, 3, 8),
                price: 24.99
            }
        ]
    },
    {
        medName: 'Akorta',
        imgUrl: 'pill2',
        pharmacies: [
            {
                name: 'OptumRx',
                town: 'Razgrad',
                expirationDate: new Date(2022, 2, 30),
                price: 7.99
            },
            {
                name: 'PharmaMed',
                town: 'DimitrovGrad',
                expirationDate: new Date(2022, 3, 8),
                price: 14.99
            }
        ]
    },
    {
        medName: 'Atoris',
        imgUrl: 'pill2',
        pharmacies: [
            {
                name: 'OptumRx',
                town: 'Razgrad',
                expirationDate: new Date(2022, 6, 30),
                price: 17.99
            },
            {
                name: 'PharmaMed',
                town: 'DimitrovGrad',
                expirationDate: new Date(2022, 10, 8),
                price: 24.99
            },
            {
                name: 'PharmaMed',
                town: 'Sofia',
                expirationDate: new Date(2022, 3, 8),
                price: 14.99
            },
            {
                name: 'PharmaMed',
                town: 'Varna',
                expirationDate: new Date(2022, 6, 8),
                price: 34.99
            }
        ]
    }
]
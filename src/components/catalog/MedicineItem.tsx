import './MedicineItem.scss';
import { Pharmacy } from "./mockData";
import { useState } from "react";

interface MedicineItemProps {
    imgUrl?: string,
    pharmacies: Pharmacy[],
    medName?: string
}

const PHARMACY_THRESHOLD = 2;
export default (props: MedicineItemProps) => {
    const [pharmacies, setPharmacies] = useState(props.pharmacies);
    const img = props.imgUrl ? require(`../../assets/pills/${props.imgUrl}.jpg`) : null;
    const notShownCount = props.pharmacies.length - PHARMACY_THRESHOLD;
    const formatDate = date => `${date.getDay()}.${date.getMonth()}.${date.getUTCFullYear()}`

    return (
        <div className='medicine'>
            <div className="image-wrapper">
                {!img
                    ? <div className='no-image'>Изображение</div>
                    : <img src={img}/>}
            </div>
            <div className="pharmacies-wrapper">
               <h3 className='medicine-name'>
                   {props.medName}
               </h3>
                {
                    props.pharmacies.slice(0, PHARMACY_THRESHOLD).map((p, i) =>
                        <div
                            key={i}
                            className="pharmacy"
                        >
                            <div className="top-row">
                                <p>Аптека: {p.name}</p>
                                <p>Град: {p.town}</p>
                            </div>
                            <div className="bot-row">
                                Срок на годност: {formatDate(new Date(p.expirationDate))}
                            </div>
                            <p className="price">{p.price}лв</p>
                        </div>)
                }
                <p className='more-pharmacies'>
                    {( notShownCount && notShownCount > 0 ) ?
                        `И още ${notShownCount} аптеки...` : null}
                </p>
            </div>
        </div>
    );
}
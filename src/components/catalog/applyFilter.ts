import { Medicine } from "./mockData";
import { MedicineFilter } from "../../reducers/medicineFilter";

function applyTownFilter(m: Medicine, town: string) {
    return !town || m.pharmacies.find(p => p.town.toLowerCase() === town.toLowerCase());
}


function applyMinPrice(m: Medicine, minPrice: number) {
    return !minPrice || (m.pharmacies = m.pharmacies.filter(p => p.price > minPrice));
}


function applyMaxPrice(m: Medicine, maxPrice: number) {
    return !maxPrice || (m.pharmacies = m.pharmacies.filter(p => p.price < maxPrice));
}

function applyStartDateFilter(m: Medicine, startDate: Date) {
    return !startDate || m.pharmacies.find(p => new Date(p.expirationDate) >= startDate);
}

function applyNameFilter(m: Medicine, name: string) {
    return !name || (m.medName?.toLowerCase().includes(name.toLowerCase()));
}

export const filterData = (data: Medicine[], filters: MedicineFilter) => {
    const { town, region, minPrice, maxPrice, medType, startDate, name, order } = filters;

    // console.log(data)
    // console.log(data.filter(m => applyTownFilter(m, town)))
    return data.filter(m => applyTownFilter(m, town)
        && applyTownFilter(m, region)
    && applyMinPrice(m, minPrice)
        && applyMaxPrice(m, maxPrice)
        && applyNameFilter(m, name)
        && applyStartDateFilter(m, startDate)
    && m.pharmacies.length > 0)
}
/*

&& ( region && applyTownFilter(m, region)
    && ( minPrice && applyMinPrice(m, minPrice)
        && ( maxPrice && applyMaxPrice(m, maxPrice) ) ) )*/

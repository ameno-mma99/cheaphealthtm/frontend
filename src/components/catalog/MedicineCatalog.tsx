import { useEffect, useState } from "react";
import BackButton from "../buttons/BackButton";
import MedicineItem from "./MedicineItem";
import { Medicine, medicineMock } from './mockData';
import Select from 'react-select';
import './MedicineCatalog.scss';
import { useWindowSize } from "../../hooks/useWindowSize";
import { useDispatch, useSelector } from "react-redux";
import medicineFilter, { updateFilter } from "../../reducers/medicineFilter";
import { filterData } from "./applyFilter";

const options = [
    { value: 'ACS', label: 'Подреди по: Най-ниска цена' },
    { value: 'DESC', label: 'Подреди по: Най-висока цена' },
]
export default () => {
    const dispatch = useDispatch();
    const [selectedOption, setSelectedOption] = useState(options[0]);
    const [medicine, setMedicine] = useState<Medicine[]>(medicineMock);
    const [itemCount, setItemCount] = useState(medicineMock.length);
    const [moreItemsShown, setMoreItemsShown] = useState(false);
    const size = useWindowSize();
    // @ts-ignore
    const appliedFilter = useSelector(state => state.medicineFilter);


    useEffect(() => {
        setMedicine(filterData(JSON.parse(JSON.stringify(medicineMock)), appliedFilter));
    }, [appliedFilter]);
    console.log(medicine)

    useEffect(() => {
        const medicineCard = document.querySelector('.medicine');
        if (!medicineCard) return;
        const { width: containerWidth } = document.querySelector('.medicine-catalog').getBoundingClientRect();
        const { width, height } = medicineCard.getBoundingClientRect();
        const { innerHeight } = window;
        const maxContainerHeight = 75 / 100 * innerHeight;
        const itemsPerRow = Math.floor(( containerWidth - 120 ) / width);
        const maxRows = Math.floor(( maxContainerHeight - 70 ) / height);
        setItemCount(maxRows * itemsPerRow);
        setMoreItemsShown(false);

    }, [size]);

    const shouldOverflow = moreItemsShown || itemCount < medicine.length;


    return (
        <div className='medicine-catalog'>
            <div className="top-medicine-row">
                <BackButton text='Back'/>
                <input
                    className='name-filter'
                    placeholder='Името на лекарството, което търсите'
                    onChange={event => dispatch(updateFilter('name', event.target.value))}
                />
                <Select
                    options={options}
                    onChange={setSelectedOption}
                    value={selectedOption}
                />
            </div>
            <div className={`container ${shouldOverflow && 'overflow'}`}>
                {medicine.length === 0 && 'No results'}
                {medicine.slice(0, itemCount).map((m, index) =>
                    <MedicineItem
                        key={index}
                        imgUrl={m.imgUrl}
                        medName={m.medName}
                        pharmacies={m.pharmacies}
                    />)}
            </div>
            {shouldOverflow && !moreItemsShown && <div className='more-results' onClick={() => {
                setItemCount(medicine.length);
                setMoreItemsShown(true);
            }}>
                Зареди още резултати
            </div>}
        </div>
    );
}
import Calendar from 'react-calendar'
import { useState } from "react";
//@ts-ignore
import CalendarImg from '../../assets/calendar.jpg';
import './styles.scss';

interface CalendarProps {
    value: any,
    onChange: (e) => void,
    label: string
}

export default (props: CalendarProps) => {
    const [isOpen, setIsOpen] = useState(false);
    return (
        <div className="calendar-wrapper">
            {isOpen && <div className="popup-wrapper">
                <Calendar
                    onChange={props.onChange}
                    value={props.value}
                />
            </div>}
            <div className="toggle" onClick={() => setIsOpen(!isOpen)}>
                <p className="label">{props.label}</p>
                <img src={CalendarImg}/>
            </div>
        </div>
    )
}
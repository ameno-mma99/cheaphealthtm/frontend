import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { toggleModal } from "../../reducers/modalReducer";
import './styles.scss';

export default (props) => {
    const dispatch = useDispatch();
    // @ts-ignore
    const modalProps = useSelector(state => state.modalReducer);
    const { isOpen, content, closeOnClickOutside, contentProps } = modalProps;
    const [isModalOpen, setIsModalOpen] = useState(isOpen);
    const ModalContent = content
    useEffect(() => {
        // window.addEventListener('click', e => {
        //     // @ts-ignore
        //     if (isOpen && e.target.closest('modal-outer') === null) {
        //         dispatch(toggleModal({
        //             ...modalProps,
        //             isOpen: false
        //         }))
        //     }
        // })
    }, []);

    useEffect(() => {
        setIsModalOpen(isOpen)
    }, [isOpen]);

    const closeModal = () => dispatch(toggleModal({ ...modalProps, isOpen: false }))


    return <div className={`modal-outer ${isModalOpen && 'open'}`}>
        <div className="modal-inner">
            <div className='modal-close-btn' onClick={closeModal}>&times;</div>
            {content && <ModalContent {...contentProps} />}
        </div>
    </div>
}
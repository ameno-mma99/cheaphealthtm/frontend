import './Button.scss';
import { Link } from "react-router-dom";

interface ButtonWithImageProps {
    text: string,
    onClick?: (e) => void,
    to?: string,
    imageUrl: string
}

export default (props: ButtonWithImageProps) => {
    const content = ( <>
        <div className="image-container">
            <img src={props.imageUrl}/>
        </div>
        <button className='image-button button'>
            {props.text}
        </button>
    </> )

    return props.to
        ? <Link to={props.to} className='button-with-image'> {content} </Link>
        : (
            <div className='button-with-image' onClick={props.onClick}>
                {content}
            </div>
        )
};
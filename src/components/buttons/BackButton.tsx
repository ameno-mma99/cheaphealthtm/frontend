import './Button.scss';

export default (props) => (
    <div className='button-noimage'>
        <button
            className='button'
            style={
                {
                    background: props.background,
                    color: props.color,
                    borderRadius: '5px',
                    margin: props.margin,
                    padding: props.padding
                }
            }
            onClick={() => window.history.back()}
        >{props.text}</button>
    </div>
);
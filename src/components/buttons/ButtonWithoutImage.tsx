import './Button.scss';
import { Link } from "react-router-dom";

export default (props) => {
    const content = <button className='button'
                            style={
                                {
                                    background: props.background,
                                    color: props.color,
                                    borderRadius: '5px',
                                    margin: props.margin,
                                    padding: props.padding
                                }
                            }>{props.text}</button>;

    return (
        <div className='button-noimage' onClick={props.onClick}>
            {props.to
                ? <Link to={props.to}>
                    {content}
                </Link>
                : content}
        </div>
    );
}

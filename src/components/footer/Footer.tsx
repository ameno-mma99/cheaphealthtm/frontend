import './Footer.scss';

export default () => (
    <div className='footer'>
        <div className='social-media'>
            <h3>Социални медии:</h3>
        </div>

        <div className='contacts'>
            <h3>Контакти:</h3>
        </div>
    </div>
);
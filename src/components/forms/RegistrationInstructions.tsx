import './Register.scss';

export default () => (
    <div className='register-instructions'>
        <div className='instructions-title'>
            <h3>Registration instructions</h3>
        </div>

        <div className='instructions-description'>
            <span>Username must have at least 8 symbols.</span>
            <br/>
            <span>Password must have at least 8 symbols
                and should match re-entered password</span>
            <br/>
            <span>Phone number has to be exactly 10 numbers and
                is required because we might contact you to
                confirm your registration or if there are any problems.</span>
            <br/>
            <span>To complete your registration, we
                need to verify your pharmacy,
                so please provide a picture of your
                valid certificate</span>
            <br/>
        </div>
    </div>
);
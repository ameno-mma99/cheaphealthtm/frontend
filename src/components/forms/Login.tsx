import './Login.scss';
import { Link, Navigate } from "react-router-dom";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loginAction } from "../../reducers/login";
import BackButton from "../buttons/BackButton";
import { NotificationType, toggleNotification } from "../../reducers/notification";
import UserDB from "../../db/UserDB";
import Input from "../input/Input";

interface LoginSingleProp {
    errorText?: string,
    value?: any | string,
}

interface LoginState {
    username: LoginSingleProp,
    password: LoginSingleProp
}

const initialState: LoginState = {
    username: {},
    password: {}
}

export default () => {
    // @ts-ignore
    const authState = useSelector(store => store.loginReducer);
    const dispatch = useDispatch();
    const [loginState, setLoginState] = useState<LoginState>(initialState);

    const { isLoggedIn } = authState;

    const handleSubmit = e => {
        e.preventDefault();

        if (!loginState.username.value || !loginState.password.value) {
            setLoginState({
                username: {
                    ...loginState.username,
                    errorText: !loginState.username.value && 'Username is required'
                },
                password: {
                    ...loginState.password,
                    errorText: !loginState.password.value && 'Password is required'
                }
            })

            return;
        }
        ;
        const foundUser = UserDB.getUser(loginState.username.value);

        if (!foundUser || foundUser.password !== loginState.password.value) {
            dispatch(toggleNotification({
                content: "Username or password is incorrect",
                type: NotificationType.ERROR,
                isOpen: true,
                closeOnClick: true
            }))

            return;
        }

        dispatch(loginAction(foundUser))
        dispatch(toggleNotification({
            content: 'Logged in successfully',
            expireTime: 3000,
            type: NotificationType.SUCCESS,
            isOpen: true,
            closeOnClick: true
        }))


    }

    const setValue = (prop, value) => setLoginState({
        ...loginState,
        [prop]: { value }
    });

    if (isLoggedIn) {
        return <Navigate to='/dashboard'/>
    }

    return (
        <div className='login-container'>
            <BackButton text='Back'/>
            <form onSubmit={handleSubmit} className='login-form'>
                <div className='inputs'>
                    <Input
                        label='Username'
                        className='username-input'
                        onChange={e => setValue('username', e.target.value)}
                        value={loginState.username.value}
                        errorText={loginState.username.errorText}
                    />
                    <Input
                        label='Password'
                        type='password'
                        className='password-input'
                        onChange={e => setValue('password', e.target.value)}
                        value={loginState.password.value}
                        errorText={loginState.password.errorText}
                    />
                    <button
                        className='button'
                        type='submit'
                    >Log in
                    </button>
                    <div className='link-wrapper'>
                        <div className='register-redirect'>
                            <Link to='/register'>Don't have an account? Register your {"\n"} pharmacy here!</Link>
                        </div>
                        <div className='forgot-redirect'>
                            <Link to='/forgot'>Forgot password?</Link>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    );
}
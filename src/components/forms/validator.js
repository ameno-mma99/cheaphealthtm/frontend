export const validator = {
    pwd: '',
    cnfPwd: '',
    username: username => {
        const isUsernameTaken = (name) => {
            const localStorageJson = localStorage.getItem('registeredUsers') || '[]';
            const allUsers = JSON.parse(localStorageJson);

            return allUsers.find(user => user.username === name);
        }

        if (!username) return 'Please fill this field';
        else if (username.length < 8) return 'Username must be at least 8 symbols';
        else if (isUsernameTaken(username)) return 'Username is already taken';

        return null;
    },
    email: email => {
        const isEmailTaken = (email) => {
            const localStorageJson = localStorage.getItem('registeredUsers') || '[]';
            const allUsers = JSON.parse(localStorageJson);

            return allUsers.find(user => user.email === email);
        }

        const emailRegex = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/;
        if (!email) return 'Please fill this field';
        else if (!emailRegex.test(email)) return 'Email not in correct format.';
        else if (isEmailTaken(email)) return 'Email is already registered';

        return null;
    },
    phone: phone => {
        const phoneRegex = /^\d{10}$/g;

        if (!phone) return 'Please fill this field';
        else if (!phoneRegex.test(phone)) return 'Phone number format not valid';

        return null;
    },
    password: function (password) {
        this.pwd = password;

        if (!password) return 'Please fill this field';
        else if (password.length < 8) return 'Password must be at least 8 symbols';

        return null;
    },
    confirmPassword: function (confirmPassword) {
        this.cnfPwd = confirmPassword;
        if (!confirmPassword) return 'Please fill this field';
        else if (confirmPassword !== this.pwd) return 'Passwords do not match';

        return null;
    }

}
import './Register.scss';
import RegistrationInstructions from "./RegistrationInstructions";
import { Link } from "react-router-dom";
import FileInput from "../input/FileInput";
import fileInput from "../input/FileInput";
import { useState } from "react";
import UserDB from "../../db/UserDB";
import { useDispatch } from "react-redux";
import { NotificationType, toggleNotification } from "../../reducers/notification";
import BackButton from "../buttons/BackButton";
import Input from "../input/Input";
import { validator } from "./validator";


interface RegisterSingleProp {
    errorText?: string,
    value?: string,
}

interface RegisterState {
    username: RegisterSingleProp,
    password: RegisterSingleProp,
    confirmPassword: RegisterSingleProp,
    email: RegisterSingleProp,
    phone: RegisterSingleProp,
    fileInput: RegisterSingleProp,
}

const initialState: RegisterState = {
    username: {},
    password: {},
    confirmPassword: {},
    email: {},
    phone: {},
    fileInput: {},
}

export default () => {
    const dispatch = useDispatch();
    const [inputState, setInputState] = useState<RegisterState>(initialState);

    function handleSubmit(e) {
        e.preventDefault();

        let user = {
            username: inputState.username.value,
            password: inputState.password.value,
            phone: inputState.phone.value,
            email: inputState.email.value,
            certificate: {
                imageSrc: '',
                name: ''
                // imageSrc: URL.createObjectURL(inputState.fileInput.value),
                // name: inputState.fileInput.value.name,
            },
            pharmacyDetails: {}
        };

        UserDB.registerUser(user);
        setInputState(initialState);
        dispatch(toggleNotification({
            content: {
                actionElement: <Link to={'/login'}>login</Link>,
                text: 'Registered successfully',
            },
            type: NotificationType.SUCCESS,
            isOpen: true,
            closeOnClick: true
        }));
    }

    const setValue = (prop, value) => setInputState({
        ...inputState,
        [prop]: {
            ...inputState[prop],
            value
        }
    });

    const setError = (prop, errorText) => setInputState({
        ...inputState,
        [prop]: {
            ...inputState[prop],
            errorText
        }
    });

    const validate = (field, value) => {
        const error = validator[field](value);
        if (error) {
            setError(field, error);
        } else {
            setError(field, null)
        }
    }

    function isAllValid() {
        const { username, password, phone, email, confirmPassword, fileInput } = inputState;
        return !username.errorText && !password.errorText && !phone.errorText
            && !email.errorText && !confirmPassword.errorText && username.value
            && password.value && phone.value && email.value && fileInput.value;
    }

    return (
        <div className='register-container'>
            <BackButton text='Back'/>
            <form onSubmit={handleSubmit} className='register-form'>
                <div className="inputs">
                    <Input
                        label='Username'
                        helperText='*Field required'
                        errorText={inputState.username.errorText}
                        value={inputState.username.value}
                        onChange={e => setValue('username', e.target.value)}
                        onBlur={e => validate('username', e.target.value)}
                    />
                    <Input
                        label='Email'
                        helperText='*Field required'
                        errorText={inputState.email.errorText}
                        value={inputState.email.value}
                        onChange={e => setValue('email', e.target.value)}
                        onBlur={e => validate('email', e.target.value)}
                    />
                    <Input
                        label='Phone'
                        helperText='*Field required'
                        errorText={inputState.phone.errorText}
                        value={inputState.phone.value}
                        onChange={e => setValue('phone', e.target.value)}
                        onBlur={e => validate('phone', e.target.value)}
                    />
                    <Input
                        type='password'
                        label='Password'
                        helperText='*Field required'
                        errorText={inputState.password.errorText}
                        value={inputState.password.value}
                        onChange={e => setValue('password', e.target.value)}
                        onBlur={e => validate('password', e.target.value)}
                    />
                    <Input
                        type='password'
                        label='Re-enter password'
                        helperText='*Field required'
                        errorText={inputState.confirmPassword.errorText}
                        value={inputState.confirmPassword.value}
                        onChange={e => setValue('confirmPassword', e.target.value)}
                        onBlur={e => validate('confirmPassword', e.target.value)}
                    />
                </div>
                <div className="file-input-wrapper">
                    <FileInput
                        onChange={file => setValue('fileInput', file)}
                        reset={!inputState.fileInput.value}
                    />

                    <button
                        className='button'
                        type='submit'
                        disabled={!isAllValid()}
                    >Register
                    </button>
                    <Link to='/login'> Already have an account? Log in from here!</Link>

                </div>
                <div className='instructions'>
                    <RegistrationInstructions/>
                </div>
            </form>

        </div>
    );
}
import "./Register.scss";
import {useState} from "react";

interface RegisterState {
    username: string,
    email: string,
    password: string,
    confirmPassword: string,
    phone: string,
    certificate: any
}

export default () => {
    const [state, setState] = useState({});

    const handleEmailChange = e => {
        const {value} = e.target;
        setState({
            ...state,
            email: value
        })
    }

    const handlePhoneChange = e => {
        const {value} = e.target;
        setState({
            ...state,
            phone: value
        })
    }

    const onPasswordChange = e => {
        const {value} = e.target;

        if(value == '') {

        }

        setState({
            ...state,
            password: value
        })

        console.log(value);
    }

    const onConfirmPasswordBlur = e => {
        const {value} = e.target;

        if(value.password == value.confirmPassword ) {

        }
        else {
            console.log("passwords not matching");
        }
    }

    const handleFileChange = e => {
        console.log(e)
        const {value} = e.target;
        setState({
            ...state,
            certificate: value
        })
    }

    // const handleFileUpload = e => {
    //     return (<input type='file' onChange={handleFileChange} hidden={true}/>)
    // }

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(state);

        const localStorageJson = localStorage.getItem('registeredUsers') || '[]';
        console.log(localStorageJson)

        const allUsers = JSON.parse(localStorageJson);
        console.log(allUsers)
        const newUsers = [
            ...allUsers,
            state
        ];

        localStorage.setItem('registeredUsers', JSON.stringify(newUsers));
    }

    return (
        <div className='register-form'>
            <form onSubmit={handleSubmit}>
                <input type='text'
                       name='username'
                       placeholder='Username'
                       onChange={(e) => setState({
                           ...state,
                           username: e.target.value
                       })}
                />
                <input type='email'
                       name='email'
                       placeholder='Email'
                       onChange={handleEmailChange}/>
                <input
                    type='text'
                    name='phone'
                    placeholder='Phone number'
                    onChange={handlePhoneChange}
                />
                <input
                    type='password'
                    name='password'
                    placeholder='Password'
                    onChange={onPasswordChange}
                />
                <input
                    type='password'
                    name='confirmPassword'
                    placeholder='Re-enter password'
                    onBlur={onConfirmPasswordBlur}
                />

                <label className="custom-file-upload">
                    Upload Certificate
                    <input
                        type='file'
                        onChange={handleFileChange}
                        style={{display: "none"}}
                    />
                </label>

                {/*<button*/}
                {/*    className='button'*/}
                {/*    type='button'*/}
                {/*    // onClick={handleFileUpload}*/}
                {/*>Upload Certificate*/}
                {/*</button>*/}

                <button
                    className='button'
                    type='submit'
                >Register
                </button>
            </form>
        </div>
    );
}

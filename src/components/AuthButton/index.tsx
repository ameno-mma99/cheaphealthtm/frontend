import { useDispatch, useSelector } from "react-redux";
// @ts-ignore
import ProfileImage from '../../assets/profileLogo.png';
import './styles.scss';
import { useEffect, useRef, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import { logoutAction } from "../../reducers/login";

let timeout;
export default () => {
    // @ts-ignore
    const authState = useSelector(store => store.loginReducer);
    const dispatch = useDispatch();
    const { pathname } = useLocation();
    const [isShown, setIsShown] = useState(false);
    const [z, setZ] = useState(-1);

    useEffect(() => {
        if (isShown && !timeout) {
            timeout = setTimeout(() => {
                timeout = null;
                clearTimeout(timeout)
                setZ(1)
            }, 200);
        }
        if (!isShown) {
            timeout = null;
            clearTimeout(timeout)
            setZ(-1);
        }
    }, [isShown]);

    const handleButtonClick = e => {
        if (e.target.closest('.drop-down') !== null) {
            return;
        }

        setIsShown(!isShown);
    }

    const performLogout = e => {
        dispatch(logoutAction());
    }


    const { authUser, isLoggedIn } = authState;
    if (!isLoggedIn) return null;

    return (
        <div className={`auth-button ${isShown && 'active'}`} onClick={handleButtonClick}>
            <div
                style={{ zIndex: z }}
                className={`drop-down ${isShown && 'shown'}`}
            >
                <ul className='menu-items'>
                    <li className={`menu-item ${pathname === '/dashboard' && 'active'}`}>
                        {pathname === '/dashboard'
                            ? 'Settings'
                            : <Link to='/dashboard'>Settings</Link>}
                    </li>
                    <hr/>
                    <li className='menu-item' onClick={performLogout}>Logout</li>
                </ul>
            </div>
            <div className='auth-button-img'>
                <img src={ProfileImage}/>
            </div>

            <div className='auth-button-text'>
                {authUser.username}
            </div>
        </div>
    );
};
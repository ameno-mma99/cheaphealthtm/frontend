//@ts-ignore
import EditImg from '../../assets/edit.png';
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useRef } from "react";
import { updateAllPharmacyDetails, updatePharmacyDetails } from "../../reducers/login";
import { toggleModal } from "../../reducers/modalReducer";
import EditProfile from "./EditProfile";
import './styles.scss';

export default () => {
    const imgRef = useRef();
    const dispatch = useDispatch();
    // @ts-ignore
    const { authUser: { pharmacyDetails, username } } = useSelector(state => state.loginReducer);

    useEffect(() => {
    }, []);

    const confirm = (image, address) => {
        if (!image) {
            dispatch(toggleModal({ isOpen: false }));
            dispatch(updateAllPharmacyDetails({ address }));

            return;
        }

        const fReader = new FileReader();
        fReader.onload = function () {
            const result = fReader.result;
            // @ts-ignore
            imgRef.current.src = result;
            dispatch(toggleModal({ isOpen: false }));
            dispatch(updateAllPharmacyDetails({ pharmacyImg: result.toString(), address }));
        };

        fReader.readAsDataURL(image);
    }

    const handleEditProfileClick = () => dispatch(toggleModal({
        isOpen: true,
        content: EditProfile,
        contentProps: {
            onConfirm: confirm,
            currentAddress: pharmacyDetails?.address,
            currentImg: pharmacyDetails?.pharmacyImg
        }
    }));

    return (
        <div className="profile-wrapper">
            <div className="pharmacy-image">
                {pharmacyDetails?.pharmacyImg && <img ref={imgRef} src={pharmacyDetails.pharmacyImg}/>}
            </div>
            <div className="pharmacy-address">
                <p className="pharmacy-name">{username}</p>
                <address>
                    {pharmacyDetails?.address
                        ? pharmacyDetails.address
                        : 'Адрес и допълнителни контакти на аптеката'}
                </address>
            </div>
            <div className="edit-btn" onClick={handleEditProfileClick}>
                <img src={EditImg} alt='edit'/>
            </div>
        </div>
    );
}
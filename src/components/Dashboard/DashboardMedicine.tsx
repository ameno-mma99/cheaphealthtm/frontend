import ButtonWithImage from "../buttons/ButtonWithImage";
//@ts-ignore
import MedWithReceipt from "../../assets/medWithReceipt.png";
//@ts-ignore
import MedNoReceipt from "../../assets/medNoReceipt.png";
export default () => {
    return (
        <div className='medicine-wrapper'>
            <ButtonWithImage
                text='Лекарства от системата'
                imageUrl={MedNoReceipt}
                to='/browse-medicine?recipe=0'
            />
            <ButtonWithImage
                text='Всички лекарства'
                imageUrl={MedWithReceipt}
                to='/browse-medicine?recipe=1'
            />
        </div>
    );
}
import './styles.scss';
import { useSelector } from "react-redux";
import { Navigate } from 'react-router-dom';
import ProfileSettings from "./ProfileSettings";
import DashboardMedicine from "./DashboardMedicine";

export default () => {
    //@ts-ignore
    const { isLoggedIn, ...rest } = useSelector(state => state.loginReducer);

    if (!isLoggedIn) return <Navigate to='/login'/>

    return (
        <div className='dashboard-container'>
            <ProfileSettings/>
            <DashboardMedicine/>
        </div>
    );
}
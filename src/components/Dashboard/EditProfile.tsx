import FileInput from "../input/FileInput";
import Input from "../input/Input";
import { useState } from "react";
import ButtonWithoutImage from "../buttons/ButtonWithoutImage";

export default (props) => {
    const [image, setImage] = useState();
    const [address, setAddress] = useState(props.currentAddress);

    return (
        <div className='edit-profile-wrapper'>
            <FileInput
                label='Качи снимка на аптеката'
                onChange={file => setImage(file)}
                value={image}
            />
            <Input
                label='Въведи адрес'
                onChange={e => setAddress(e.target.value)}
                value={address}
            />
            <ButtonWithoutImage
                text='Потвърди'
                onClick={e => props.onConfirm(image, address)}
                />
        </div>
    )
}
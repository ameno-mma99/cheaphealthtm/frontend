import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {toggleNotification} from "../../reducers/notification";
import './styles.scss';


export default () => {
    const dispatch = useDispatch();
    // @ts-ignore
    const reduxState = useSelector(state => state.notificationReducer);
    const {content, type, expireTime, isOpen, closeOnClick} = reduxState;

    const [isShown, setIsShown] = useState(isOpen);
    useEffect(() => setIsShown(isOpen), [isOpen]);

    const openClassName = isShown ? 'shown' : 'hidden';

    if (expireTime) {
        setTimeout(() => hideNotification(), expireTime);
    }

    const hideNotification = () => {
        dispatch(toggleNotification({
            ...reduxState,
            expireTime: null,
            isOpen: false
        }))
    }

    return (
        <div className={`notification notification-${type} ${openClassName}`}
             onClick={() => closeOnClick && hideNotification()}>
            <span className='close-btn' onClick={hideNotification}>&times;</span>
            <div className='notification-content'>
                {(content && content.text && content.actionElement)
                    ?
                    <>
                        <div className='notification-text'>
                            {content.text}
                        </div>
                        <div className='notification-action'>
                            {content.actionElement}
                        </div>
                    </>
                    :
                    <div className='notification-text'>
                        {content}
                    </div>
                }
            </div>
        </div>
    );
}
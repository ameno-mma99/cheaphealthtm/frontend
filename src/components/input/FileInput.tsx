// @ts-nocheck
import {LegacyRef, MutableRefObject, RefObject, useEffect, useRef, useState} from "react";
import './fileInputStyles.scss';


export default (props) => {
    const inputRef = useRef();
    const [preview, setPreview] = useState();
    const [fileName, setFileName] = useState();

    useEffect(() => {
        if (props.reset) {
            setPreview(null);
            setFileName(null);
        }
    }, [props.reset]);


    function handleFileChange(e) {

        const file = e.target.files[0];
        setPreview(URL.createObjectURL(file));
        setFileName(file.name);
        props.onChange(file);
    }

    return <div className='wrapper file-input'>
        <div className='image-preview'>
            {preview && <img src={preview}/>}
        </div>
        <div className='button-wrapper'>
            <div className='image-text'>
                {fileName}
            </div>
            <input type='file' hidden ref={inputRef} onChange={handleFileChange}/>
            <button
                className='file-input-button'
                onClick={e => {
                    e.preventDefault();
                    inputRef.current.click();
                }}
            > {props.label || 'Upload certificate'}
            </button>
        </div>
    </div>
}
// @ts-nocheck
import './styles.scss';
import {useEffect, useState} from "react";

export interface InputProps {
    hasError?: boolean,
    errorText?: string,
    label: string,
    placeholder?: string,
    value?: string,
    helperText?: string,
    onFocus?: (e) => void,
    onBlur?: (e) => void,
    onChange?: (e) => void,
    type?: string,
    className?: string
}

export default (props: InputProps) => {
    const [value, setValue] = useState(props.value || '');
    const hasError = props.hasError || !!props.errorText;

    useEffect(() => {
        setValue(props.value || '')
    }, [props.value]);

    if (props.type === null) {
        props.type = 'text';
    }

    return (
        <div className='outer-wrapper'>
            <div className={hasError ? 'has-error inner-wrapper' : 'inner-wrapper'}>
                <label className={value && 'elevated'}>{props.label}</label>
                <input
                    value={value}
                    type={props.type}
                    onChange={e => {
                        setValue(e.target.value);
                        props.onChange(e);
                    }}
                    onBlur={props.onBlur}
                    onFocus={props.onFocus}
                />
            </div>
            <div className={hasError ? 'below-text has-error' : 'below-text'}>
                {hasError ? props.errorText : props.helperText}
            </div>
        </div>
    )
}